export const TOGGLE_GREETING = 'TOGGLE_GREETING';

export const changeGreeting = () => (dispatch, getState) => {
    if (getState().greeting.text === 'Hello World!') {
        dispatch({
            type: TOGGLE_GREETING,
            payload: 'Hello Moon!'
        });
    } else {
        dispatch({
            type: TOGGLE_GREETING,
            payload: 'Hello World!'
        });
    }
};